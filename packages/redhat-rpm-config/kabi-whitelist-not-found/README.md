# packages/redhat-rpm-config/kabi-whitelist-not-found test
Test for BZ#1126086 (KERNEL ABI COMPATIBILITY WARNING when building any)

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
